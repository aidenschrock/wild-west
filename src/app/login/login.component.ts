import { Component } from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  isShown: boolean = false ; // hidden by default

  toggleShow() {
    console.log(this.isShown)

  this.isShown = !this.isShown;
  console.log(this.isShown)

  }

}
