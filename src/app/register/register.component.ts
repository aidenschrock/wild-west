import { Component} from '@angular/core';
import tos from './terms-of-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  isShown: boolean = false ; // hidden by default

  toggleShow() {
    console.log(this.isShown)

  this.isShown = !this.isShown;
  console.log(this.isShown)

  }




  termsOfService=tos;

  // disableSubmit() {
  //   document.getElementById("submit").disabled = true;
  //  }

  //   activateButton(element) {

  //       if(element.checked) {
  //         document.getElementById("submit").disabled = false;
  //        }
  //        else  {
  //         document.getElementById("submit").disabled = true;
  //       }

  //   }
    isEnabled: boolean = false ; //disabled by default

    toggleEnable() {
      console.log(this.isEnabled)
      this.isEnabled = !this.isEnabled;
      console.log(this.isEnabled)
    }

    submit() {
      console.log('submit enabled')

    }
}
