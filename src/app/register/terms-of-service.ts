
export default `<p><strong>Last updated: August 5, 2020</strong>  </p>

<h2 id="welcometorecolude"><strong><em>Welcome to Recolude!</em></strong></h2>

<p>Recolude is a SaaS-based platform (as modified from time to time, the “Recolude Platform”) that enables developers (“Developers” or “you”) to store and host their recordings and 3D models (collectively, the “Recordings”) made inside a 3D engine for their projects (the “Projects”). The website at recolude.com (the “Site”) and the various other related services, premium and other features, functions, software, applications and websites (together with the Recolude Platform and the Site, collectively the “Recolude Services”) are provided and operated, and are being made available to you and the other Developers, and the other users of any of the Recolude Services (collectively, “Users”) by Recolude, LLC (“Recolude”, “us” or “we”). All defined terms used herein shall have the meanings prescribed to these terms in these Terms of Service.  </p>

<p><strong>IMPORTANT! THESE TERMS OF SERVICE (“TERMS”) GOVERN YOUR USE OF THE SITE, THE RECOLUDE PLATFORM AND THE OTHER RECOLUDE SERVICES. IF YOU ARE AGREEING TO THESE TERMS ON BEHALF OF YOUR ORGANIZATION, REFERENCES TO “YOU” OR “YOUR” SHALL MEAN YOU, YOUR ORGANIZATION, AND ANY OTHER USER ACCESSING AND USING THE RECOLUDE SERVICES ON BEHALF OF SUCH ORGANIZATION. BY CLICKING “I AGREE”, DOWNLOADING, USING, CONFIGURING OR ACCESSING THE SITE, THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES OR OTHERWISE SIGNIFYING YOUR ACCEPTANCE OF THESE TERMS, YOU REPRESENT AND WARRANT THAT (A) YOU ARE AUTHORIZED TO ENTER THESE TERMS OF SERVICE FOR AND ON BEHALF OF YOURSELF (AND YOUR ORGANIZATION), AND ARE DOING SO, (B) YOU (AND YOUR ORGANIZATION) CAN LEGALLY ENTER INTO THESE TERMS AND (C) YOU HAVE READ AND UNDERSTAND AND AGREE THAT YOU (AND YOUR ORGANIZATION) AND EACH USER SHALL BE BOUND BY THESE TERMS OF SERVICE AND RECOLUDE’S PRIVACY POLICY (HTTPS:// RECOLUDE.COM/PRIVACY-POLICY/)(THE “PRIVACY POLICY”) AND ALL MODIFICATIONS AND ADDITIONS PROVIDED FOR. IF YOU DO NOT AGREE TO THESE TERMS OF SERVICE OR THE PRIVACY POLICY, PLEASE DO NOT USE THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES.</strong>  </p>

<p>We reserve the right to provide separate terms of service for specific activities and programs (e.g., competitions, etc.) (collectively, the “Specific Terms”). If so, to the extent that the Specific Terms conflict with these Terms, the Specific Terms shall apply.  </p>

<p>These Terms contain an Arbitration provision which will, with limited exception, require you to submit disputes you have against Recolude to binding and final arbitration. You will only be permitted to pursue claims against Recolude on an individual basis, not as a plaintiff or class member in any class or representative action or proceeding; and you will only be permitted to seek relief (including monetary, injunctive, and declaratory relief) on an individual basis.  </p>

<h2><strong><a id="eligibility">1. Eligibility</a></strong></h2>

<p>To access and use the Recolude Platform, the Site and the other Recolude Services, you must be at least 18 years of age. <strong>BY CLICKING THE “I AGREE” BUTTON, BY ACCESSING OR USING THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES, YOU REPRESENT THAT:</strong></p>

<p><strong>YOU HAVE NOT BEEN PREVIOUSLY SUSPENDED OR REMOVED FROM THE SITE, THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES; <br />
YOU CONFIRM THAT YOU ARE OF LEGAL AGE TO FORM A BINDING CONTRACT WITH RECOLUDE; <br />
YOU WILL COMPLY WITH THESE TERMS AND ALL APPLICABLE LOCAL, STATE, NATIONAL AND INTERNATIONAL LAWS, RULES AND REGULATIONS; AND <br />
YOU ARE NOT A COMPETITOR OF RECOLUDE AND DO NOT INTEND TO USE THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES FOR REASONS THAT ARE IN COMPETITION WITH RECOLUDE OR OTHERWISE TO REPLICATE SOME OR ALL OF THE OTHER RECOLUDE SERVICES FOR ANY REASON.</strong>  </p>

<h2 id="2privacy"><strong>2. Privacy</strong></h2>

<p>Your privacy is important to Recolude. Our goal is to make the Recolude Platform and the other Recolude Services as good, useful and rewarding for you as possible. In order to do that, Recolude may collect and process information from you when you use the Recolude Platform or any of the other Recolude Services. Recolude will collect certain personally identifiable information from you as set forth in more detail in our Privacy Policy. By accessing or using the Recolude Platform or any of the other Recolude Services, you agree that Recolude may collect, use and disclose, as set forth in the Privacy Policy, the information you provide when you access and use the Recolude Platform or any of the other Recolude Services, and in some cases, information that is provided by or through any of the Recolude Services.  </p>

<h2 id="3therecoludeplatform"><strong>3. The Recolude Platform</strong></h2>

<h3 id="31accessingandusingtherecoludeplatform"><strong>3.1 Accessing and Using the Recolude Platform</strong></h3>

<p><strong>(a) For Developers</strong> </p>

<p>(i) To access and use the Recolude Platform, you must first register and create an account and pay the applicable subscription fees (as described below)  </p>

<p>(ii) After creating an account, you must register each of your Projects. Once you have registered your Project you will received certain API keys that will enable you to talk with the Recolude Platform from your application.  </p>

<p>(iii) In registering and creating an account, you agree to (i) provide true, accurate, current, and complete information about yourself and your Project as prompted during the registration and account creation process (“Registration Data”), and (ii) maintain and promptly update the Registration Data, including your PayPal information, to keep it true, accurate, current and complete. You are responsible for all activities that occur under your account. If you provide any information that is untrue, inaccurate, not current, or incomplete, or Recolude has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, Recolude has the right to suspend or terminate your account and prohibit you from accessing and using the Recolude Platform and/or any of the other Recolude Services. You agree not to create an account using a false identity or information, or on behalf of someone other than yourself. You agree that you shall not have more than one account. You agree not to create an account or to access and use the Recolude Platform or any of the other Recolude Services if you have been previously removed by Recolude or if you have been previously banned from accessing and using the Recolude Platform or any of the other Recolude Services.  </p>

<p>(iv) You have the ability to designate your Recording as “public.” If you elect to do that, any User, whether or not he or she has registered and has an account, can access your Recording.  </p>

<p><strong>(b) For other Users</strong></p>

<p>Users other than Developers do not need to register or create an account in order to view or listen to a Recording that has been designated as “public” by the Developer(s).  </p>

<h3 id="323dengines"><strong>3.2 3D Engines</strong></h3>

<p>The Recolude Platform is not compatible with all 3D engines. The current list of 3D engines that are compatible with the Recolude Platform are listed at https://recolude.com/integrations/. If you have any questions whether the 3D engine that you are working with is compatible with the Recolude Platform, please contact us at support@recolude.com.  </p>

<h3 id="33maintainingcopiesofrecordings"><strong>3.3 Maintaining copies of Recordings</strong></h3>

<p>It is your responsibility to maintain copies of your Recordings. Recolude will use commercially reasonable efforts to prevent loss of the Recordings in accordance with good industry practice. However, despite our efforts, no security measures are 100% effective and Recolude cannot (a) ensure or warrant the security of the Recordings or (b) guarantee that (i) there will be no loss of data or (ii) access to the Recordings will be available without interruption.  </p>

<h2 id="4feespaymentsandrefunds"><strong>4. Fees, Payments and Refunds</strong></h2>

<h3 id="41fees"><strong>4.1 Fees</strong></h3>

<p>To be able to store and host your Recordings through the Recolude Platform, you must pay the subscription fees associated with the subscription period that you have selected (e.g., monthly or annually) (as modified from time to time, the “Subscription Fees”). The amount of the Subscription Fee will vary based on the size of the Recording that is stored through the Recolude Platform. If you exceed the storage limit associated with the Subscription Fee that you paid, you will need to pay the higher Subscription Fee associated with the size of your Recording. The current Subscription Fees can be found at https:// recolude.com/pricing/.  </p>

<h3 id="42paymentandtaxes"><strong>4.2 Payment and Taxes</strong></h3>

<p><strong>(a) Payments</strong></p>

<p>Unless otherwise agreed by Recolude, the Subscription Fees shall be paid in advance in accordance with Section 4.2(b). You authorize Recolude to automatically charge your credit or debit card through the Payment Processor for any such Subscription Fee.  </p>

<p><strong>(b) Payment Processing</strong></p>

<p>The Subscription Fees and other payments made in connection with the Recolude Platform and the other Recolude Services shall be made through a third-party payment processor directed by Recolude (e.g., PayPal, Stripe, etc.) (any such third-party payment processor, the “Payment Processor”). Payment processing services for the Recolude Platform are currently provided by Stripe and are subject to Stripe’s terms of service (as modified from time to time, the “Stripe Terms”). By agreeing to these Terms or continuing to access or use the Recolude Platform or any of the other Recolude Services, you agree to be bound by the Stripe Terms. As a condition of Recolude’s enabling payment processing services through a Payment Processor, you agree to provide the Payment Processor accurate and complete information about you, your credit or debit card or bank account and such other information required by the Payment Processor, and you authorize Recolude to share any relevant transaction information to the Payment Processor.  </p>

<p><strong>(c) Right to Suspend</strong></p>

<p>If you fail to pay any Subscription Fees when due, Recolude may, upon prior written notice to you, suspend your ability to access and use the Recolude Platform, including your Recording .  </p>

<p><strong>(d) Future Services</strong></p>

<p>In paying the Subscription Fee, you acknowledge and agree that you are not relying on future availability of the Recolude Platform beyond the subscription period for which the Subscription Fee that you paid applies.  </p>

<h3 id="43changesinfeesandpaymentmethods"><strong>4.3 Changes in Fees and Payment Methods</strong></h3>

<p>Recolude reserves the right at any time to change the Subscription Fees (including to begin charging for services that it is currently providing free of charge or for new premium features) and billing methods, either immediately upon posting on the Recolude Platform, the Site, any of the other Recolude Services or by notifying you by email.  </p>

<h3 id="44refunds"><strong>4.4 Refunds</strong></h3>

<p>ALL SUBSCRIPTION FEES ARE NON-REFUNDABLE, NON-CANCELLABLE AND NON-CREDITABLE UNLESS OTHERWISE REQUIRED BY LAW OR AGREED TO BY RECOLUDE IN ITS SOLE DISCRETION.  </p>

<h2 id="5righttoaccessanduseunauthorizeduse"><strong>5. Right to Access and Use; Unauthorized Use</strong></h2>

<h3 id="51righttoaccessanduse"><strong>5.1 Right to Access and Use</strong></h3>

<p>Subject to your compliance with all of the terms and conditions set out in these Terms, Recolude hereby grants to you a limited, non-exclusive, non-transferable, freely revocable right to access and use the Recolude Platform, the Site, and the other Recolude Services, to the extent of, and in accordance with, these Terms.  </p>

<h3 id="52preventionofunauthorizeduse"><strong>5.2 Prevention of Unauthorized Use</strong></h3>

<p>Recolude reserves the right to exercise whatever lawful means it deems necessary to prevent the unauthorized access or use of the Recolude Platform or the circumvention of the other Recolude Services, including, but not limited to, technological barriers, IP mapping, and directly contacting your Internet Service Provider (ISP) regarding such unauthorized use.  </p>

<h2 id="6additionalpolicies"><strong>6. Additional Policies</strong></h2>

<p>When accessing or using the Recolude Platform or any of the other Recolude Services, you may be subject to any additional posted policies, guidelines or rules applicable to the Recolude Platform, the Site and the other Recolude Services which may be posted from time to time (as modified from time to time, the “Policies”). All such Policies are hereby incorporated by reference into these Terms.  </p>

<h2 id="7respectingotherpeoplesrights"><strong>7. Respecting other People’s Rights</strong></h2>

<p>Recolude respects the rights of others and so should you. You therefore shall not:  </p>

<p>-violate or infringe someone else’s rights of publicity, privacy, copyright, trademark, or other intellectual property right;  </p>

<p>-post anything that is unlawful, threatening, abusive, harassing, defamatory, libelous, deceptive, fraudulent, invasive of another’s privacy, tortious, obscene, vulgar, pornographic, offensive, profane, contains or depicts nudity, contains or depicts sexual activity, or is otherwise inappropriate as determined by Recolude in its sole discretion;  </p>

<p>-post anything that is false, misleading, untruthful or inaccurate or that includes anyone’s personal information; or  </p>

<p>-impersonate any person or entity, including any of Recolude’s employees or representatives or any other Developers or Users.</p>

<h2 id="8modificationoftheseterms"><strong>8. Modification of these Terms</strong></h2>

<p>Recolude reserves the right, at its sole discretion, to change, modify, add, or remove portions of these Terms at any time by posting the amended Terms to the Site or any of the other Recolude Services. If Recolude updates these Terms, it will update the “last updated” date at the top of the Terms. Please check these Terms, including any Policies, periodically for changes. Your continued use of the Recolude Platform or any of the other Recolude Services after the posting of changes constitutes your binding acceptance of such changes. In the event of a change to these Terms that materially modifies your rights or obligations (including applicable fees), Recolude will use commercially reasonable efforts to notify you of such change. Recolude may provide notice through a pop-up or banner within any of the Recolude Services, by sending an email to any address you may have used to register for an account, or through other similar mechanisms. Additionally, if the changed Terms materially modify your rights or obligations, Recolude may require you to provide consent by accepting the changed Terms. If Recolude requires your acceptance of the changed Terms, changes are effective only after your acceptance. For all other changes, except as stated elsewhere by Recolude, such amended Terms or fees will automatically be effective, replacing the previously-effective Terms or fees, thirty (30) days after they are initially posted on any of the Recolude Services. <strong>IF AT ANY TIME YOU DO NOT AGREE TO THESE TERMS, PLEASE IMMEDIATELY TERMINATE YOUR USE OF ALL RECOLUDE SERVICES.</strong>  </p>

<p>To the extent that any modifications to the Terms or Policies are not allowed under applicable laws, the prior most recent version of the Terms or Policies shall continue to apply.  </p>

<h2 id="9digitalmillenniumcopyrightact"><strong>9. Digital Millennium Copyright Act</strong></h2>

<p>It is Recolude’s policy to respond to notices of alleged copyright infringement that comply with the Digital Millennium Copyright Act. For more information, please go to Recolude’s DMCA Notification Policy at https://recolude.com/DMCA/. If you file a notice with our copyright agent, it must comply with the requirements set forth in 17 U.S.C. § 512(c)(3). Recolude reserves the right to terminate without notice any Developer’s or other User’s access to the Recolude Platform, the Site and the other Recolude Services if that Developer or other User is determined by Recolude to be a “repeat infringer.” In addition, Recolude accommodates and does not interfere with standard technical measures used by copyright owners to protect their materials.  </p>

<h2 id="10content"><strong>10. Content</strong></h2>

<h3 id="101limitedlicensegranttorecolude"><strong>10.1 Limited License Grant to Recolude</strong></h3>

<p>The Recolude Platform permits Developers to upload their Recordings, whether or not made available to other Developers or other Users. You will own any Recordings that you upload to the Recolude Platform. However, by uploading, providing, posting, distributing or disseminating any Recordings to or through the Recolude Platform, you hereby grant to Recolude a worldwide, non-exclusive, perpetual, irrevocable, transferable, sublicensable (through multiple tiers), assignable, fully paid-up, royalty-free, license to host, display and perform any Recordings (and any copyrights, publicity, database and other proprietary rights therein) on the Recolude Platform in accordance with these Terms and the authorizations that you have established.  </p>

<h3 id="102responsibilityforrecordings"><strong>10.2 Responsibility for Recordings</strong></h3>

<p>You are solely responsible for the accuracy, quality, integrity, legality, reliability, and appropriateness of all of your Recordings and the consequences of uploading any Recordings. By uploading your Recordings, you affirm, represent, and warrant that: (a) you are the creator and owner of or have the necessary licenses, rights, consents, and permissions to upload your Recordings; (b) your Recordings do not and will not: (i) infringe, violate, or misappropriate any third-party right, including any copyright, trademark, patent, trade secret, moral right, privacy right, right of publicity, or any other intellectual property or proprietary right or (ii) slander, defame, libel, or violate or invade the right of privacy, publicity or other rights of any person or entity; and (iii) your Recordings do not contain any viruses, adware, spyware, worms, or other malicious code or any content or file that provides a method to access to potentially infringing content outside of any of the Recolude Services. Violators of these third-party rights may be subject to criminal and civil liability. Recolude reserves all rights and remedies against any Developers who violate these Terms.  </p>

<h3 id="103accesstorecordingsbyotherdevelopersandusers"><strong>10.3 Access to Recordings by other Developers and Users</strong></h3>

<p>You hereby consent for other Developers and Users to access your Recordings to the extent authorized by you in the manner contemplated by these Terms and the Recolude Platform.  </p>

<h3 id="104disclaimer"><strong>10.4 Disclaimer</strong></h3>

<p>You understand that when using the Recolude Platform or any of the other Recolude Services, you may be exposed to Recordings or other materials from a variety of sources, and that Recolude is not responsible for the accuracy, usefulness, or intellectual property rights of or relating to such Recordings and/or other content. You further understand and acknowledge that you may be exposed to Recordings and other materials that are inaccurate, offensive, indecent, or objectionable, and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against Recolude with respect thereto. Recolude does not endorse any Recordings and other material or any opinion, recommendation or advice expressed therein, and Recolude expressly disclaims any and all liability in connection with any such Recordings and/or other content. If notified by a Developer or a content owner of any Recordings or other content or materials that allegedly do not conform to these Terms, Recolude may investigate the allegation and determine what recourse (if any) it might have. For clarity, Recolude does not permit copyright infringing activities on or through any of the Recolude Services.  </p>

<h2 id="prohibitedconduct"><strong>11. Prohibited Conduct</strong></h2>

<p>BY ACCESSING OR USING THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES YOU AGREE NOT TO:  </p>

<p><strong>11.1</strong> Decipher, decompile, disassemble, reverse engineer, modify, translate, or otherwise attempt to derive source code, algorithms, tags, specifications, architectures, structures or other elements of the Recolude Platform or any of the other Recolude Services, in whole or in part (except to the extent that the laws of your jurisdiction make such restrictions unenforceable);  </p>

<p><strong>11.2</strong> Access or use the Recolude Platform or any of the other Recolude Services for the benefit of anyone other than yourself except in accordance with these Terms, including selling, reselling or distributing, in whole or in part, the Recolude Platform as part of a service bureau or outsourcing offering;  </p>

<p><strong>11.3</strong> Provide any services, directly or indirectly, that are similar to, or based on, the Recolude Platform or any of the other Recolude Services;  </p>

<p><strong>11.4</strong> Copy, disclose, or distribute any Recordings or data or other information available through the Recolude Platform or any of the other Recolude Services, including any information, in any medium, including without limitation on the Site, by any automated or non-automated “scraping;”  </p>

<p><strong>11.5</strong> Interfere with, circumvent or disable any security or other technological features or measures of any of the Recolude Services or attempt to gain unauthorized access to any of the Recolude Services or its related systems or networks;  </p>

<p><strong>11.6</strong> Make unsolicited offers, advertisements, or proposals, or send junk mail or spam to other Developers or Users (including, but not limited to, unsolicited advertising, promotional materials or other solicitation material, bulk mailing of commercial advertising, chain mail, informational announcements, charity requests, and petitions for signatures);  </p>

<p><strong>11.7</strong> Use bots or other automated methods to: access the Recordings, the Recolude Platform, any of the Recolude Content and/or any of the Recolude Services, download any information, send or redirect messages or perform any other activities through any of the Recolude Services;  </p>

<p><strong>11.8</strong> Take any action that Recolude determines, in its sole discretion, imposes or may impose, an unreasonable or disproportionately large load on its infrastructure;  </p>

<p><strong>11.9</strong> Upload invalid data, viruses, worms or other software agents through the Recolude Platform and/or any of the other Recolude Services;  </p>

<p><strong>11.10</strong> Collect or derive any personally identifiable information, including names, email addresses from the Recolude Platform and/or any of the other Recolude Services except as may be expressly contemplated by these Terms and the Privacy Policy;  </p>

<p><strong>11.11</strong> Impersonate any person or entity, use a fictitious name, or misrepresent your affiliation with a person or entity;  </p>

<p><strong>11.12</strong> Use the Recolude Platform or any of the other Recolude Services for any unlawful or inappropriate activities, such as gambling, obscenity, pornography, violence, transmission of deceptive messages, or harassment; or  </p>

<p><strong>11.13</strong> Use the Recolude Services for any commercial solicitation purposes.  </p>

<h2 id="12thirdpartysites"><strong>12. Third-Party Sites</strong></h2>

<p>The Recolude Platform and the other Recolude Services may now or in the future include links or references to other web sites or services (“Third-Party Sites”) solely as a convenience to Developers and other Users. These links may include invoicing and payment processing, the sites of the 3D engines with which we integrate, demonstrations of our products, social media marketing and other sites or services that we think may be of interest to our Developers and other Users. Recolude does not endorse any such Third-Party Sites or the services, information, materials, products, or services contained on or accessible through Third-Party Sites. In addition, your correspondence or business dealings with, or participation in promotions of, advertisers found on or through any of the Recolude Services are solely between you and such advertiser. Access and use of Third-Party Sites, including the information, materials, products, and services on or available through Third-Party Sites are solely at your own risk.  </p>

<h2 id="13mobileanddataservices"><strong>13. Mobile and Data Services</strong></h2>

<p>You are responsible for any mobile and data charges that you may incur for using the Recolude Platform or any of the other Recolude Services. If you’re unsure what those charges may be, you should ask your service provider before using any of the Recolude Services.  </p>

<h2 id="14terminationtermsofserviceviolations"><strong>14. Termination; Terms of Service Violations</strong></h2>

<p><strong>14.1 Recolude</strong> </p>

<p>You agree that Recolude in its sole discretion, for any or no reason, and without penalty, may terminate your access and use of the Recolude Platform or any of the other Recolude Services or any account (or any part thereof) that you may have with Recolude and remove and discard all or any part of your account, user profile, at any time. Recolude may also in its sole discretion and at any time prohibit you from accessing and using the Recolude Platform or discontinue providing access to the Recolude Platform or any of the other Recolude Services, or any part thereof, with or without notice. You agree that any termination of your ability to access or use the Recolude Platform or any of the other Recolude Services or any account you may have or portion thereof may be effected without prior notice, and you agree that Recolude will not be liable to you or any third party for any such termination. Recolude reserves the right to fully cooperate with any law enforcement authorities or court order requesting or directing Recolude to disclose the identity of anyone posting any e-mail messages, or creating any Recordings that are believed to violate these Terms. Any suspected fraudulent, abusive or illegal activity may be referred to appropriate law enforcement authorities. These remedies are in addition to any other remedies that Recolude may have at law or in equity. As provided herein, Recolude does not permit copyright infringing activities on the Recolude Platform, the Site or any of the other Recolude Services, and Recolude shall be permitted to terminate access to the Recolude Platform or any of the other Recolude Services. BY ACCEPTING THESE TERMS, YOU WAIVE AND SHALL HOLD RECOLUDE HARMLESS FROM ANY CLAIMS RESULTING FROM ANY ACTION TAKEN BY RECOLUDE DURING OR AS A RESULT OF ITS INVESTIGATIONS AND/OR FROM ANY ACTIONS TAKEN AS A CONSEQUENCE OF INVESTIGATIONS BY EITHER RECOLUDE OR LAW ENFORCEMENT AUTHORITIES.  </p>

<p><strong>14.2 You</strong>  </p>

<p>Your only remedy with respect to any dissatisfaction with (i) the Recolude Platform or any of the other Recolude Services, (ii) any term of these Terms, (iii) any policy or practice of Recolude in operating the Recolude Platform or the other Recolude Services, or (iv) any Recordings viewable through the Recolude Platform, is to terminate your use of the Recolude Platform, any of the other Recolude Services and your account (if any). You may terminate your use of the Recolude Services and your account (if any) at any time.  </p>

<p><strong>14.3</strong> Effect of Termination. If at any time you terminate your use of the Recolude Platform for any reason (as manifested by your failure to pay the applicable Subscription Fees or otherwise) (the “Termination Date”), you must promptly download a copy of your Recordings. Recolude shall have no obligation to continue to store your Recordings beyond forty-five (45) days after the Termination Date.  </p>

<h2 id="15ownershipfeedback"><strong>15. Ownership; Feedback</strong></h2>

<p>Recolude owns all right, title and interest in the Recolude Platform, the Site and the other Recolude Services. The visual interfaces, graphics, design, compilation, information, computer code (including source code or object code), products, software, services, and all other elements of the Recolude Services provided by Recolude (the “Materials”) are protected by United States copyright, trade dress, patent, and trademark laws, international conventions, and all other relevant intellectual property and proprietary rights, and applicable laws. All Materials contained on the Recolude Platform or any of the other Recolude Services are the property of Recolude or its subsidiaries or affiliated companies, and/or third-party licensors. All trademarks, service marks, and trade names are proprietary to Recolude or its affiliates and/or third-party licensors. Except as expressly authorized by Recolude, you agree not to sell, license, distribute, copy, modify, publicly perform or display, transmit, publish, edit, adapt, create derivative works from, or otherwise make unauthorized use of the Materials. Recolude reserves all rights not expressly granted in these Terms.  </p>

<p>You grant to Recolude a royalty-free, worldwide, irrevocable, perpetual license to use, publish, edit, translate, distribute, display and incorporate any ratings, comments, suggestions, feedback, improvement requests or other recommendations you provide relating to the Recolude Platform or any of the other Recolude Services without restriction (“Feedback”). You should not submit any Feedback to us that you do not wish to license to us as stated above. We have no obligation (a) to maintain any Feedback in confidence; (b) to pay any compensation for any Feedback; or (c) to respond to any Feedback. You grant us the right to use the name that you submit in connection with any Feedback.  </p>

<h2 id="16indemnification"><strong>16. Indemnification</strong></h2>

<p>You agree to indemnify, save, and hold Recolude its affiliated companies, contractors, employees, agents and its third-party suppliers, licensors, and partners harmless from any third-party claims, losses, damages, or liabilities, including legal fees and expenses, arising out of your use or misuse of the Recolude Platform, the Site or any of the other Recolude Services, any Recordings, any violation by you of these Terms, or any breach of the representations, warranties, and covenants made by you herein. Recolude reserves the right, at your expense, to assume the exclusive defense and control of any matter for which you are required to indemnify Recolude and you agree to cooperate with Recolude’s defense of these claims. Recolude will use reasonable efforts to notify you of any such claim, action, or proceeding upon becoming aware of it.  </p>

<h2 id="17nowarrantiesdisclaimers"><strong>17. No Warranties; Disclaimers</strong></h2>

<h3 id="171nowarranties"><strong>17.1 No Warranties</strong></h3>

<p>TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, RECOLUDE AND ITS AFFILIATES, CONTRACTORS, DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, THIRD-PARTY PARTNERS, LICENSORS AND SUPPLIERS (COLLECTIVELY, THE “RECOLUDE PARTIES”) DISCLAIM ALL WARRANTIES, STATUTORY, EXPRESS OR IMPLIED WITH RESPECT TO THE RECOLUDE PLATFORM, THE SITE AND THE OTHER RECOLUDE SERVICES, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF PROPRIETARY RIGHTS AND WARRANTIES ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE. WITHOUT LIMITATION TO THE FOREGOING, RECOLUDE PROVIDES NO WARRANTY OR UNDERTAKING, AND MAKES NO REPRESENTATION OF ANY KIND, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, THAT THE RECOLUDE PLATFORM, THE SITE OR THE OTHER RECOLUDE SERVICES, INCLUDING, WILL MEET YOUR REQUIREMENTS OR ACHIEVE ANY INTENDED RESULTS. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM THE RECOLUDE PARTIES OR THROUGH THE RECOLUDE PLATFORM, THE SITE OR THE OTHER RECOLUDE SERVICES WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED HEREIN.  </p>

<h3 id="172asisandasavailableandwithallfaults"><strong>17.2 “As Is” and “As Available” and “With all Faults”</strong></h3>

<p>YOU EXPRESSLY AGREE THAT THE RECOLUDE PLATFORM, THE SITE AND ANY OF THE OTHER RECOLUDE SERVICES, ANY DATA, ASSESSMENTS, RESULTS, INFORMATION, THIRD-PARTY SOFTWARE, CONTENT, THIRD-PARTY SITE, SERVICES, OR APPLICATIONS MADE AVAILABLE IN CONJUNCTION WITH OR THROUGH THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE”, “WITH ALL FAULTS” BASIS AND WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND EITHER EXPRESS OR IMPLIED.  </p>

<h3 id="173recoludeplatformoperationsandcontent"><strong>17.3 Recolude Platform Operations and Content</strong></h3>

<p>THE RECOLUDE PARTIES DO NOT WARRANT THAT THE RECORDINGS, DATA, RESULTS, CONTENT, FUNCTIONS, OR ANY OTHER INFORMATION OFFERED ON OR THROUGH THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES, OR ANY THIRD-PARTY SITE WILL BE UNINTERRUPTED, OR FREE OF ERRORS, VIRUSES OR OTHER HARMFUL COMPONENTS AND DO NOT WARRANT THAT ANY OF THE FOREGOING WILL BE CORRECTED.  </p>

<h3 id="174accuracy"><strong>17.4 Accuracy</strong></h3>

<p>EXCEPT AS SPECIFICALLY PROVIDED IN WRITING BY RECOLUDE, THE RECOLUDE PARTIES DO NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE RECOLUDE PLATFORM, THE AND THE OTHER RECOLUDE SERVICES OR ANY THIRD-PARTY SITE IN TERMS OF CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE.  </p>

<h3 id="175harmtoyourcomputer"><strong>17.5 Harm to Your Computer</strong></h3>

<p>YOU UNDERSTAND AND AGREE THAT YOU USE, ACCESS, DOWNLOAD, OR OTHERWISE OBTAIN CONTENT, INFORMATION, MATERIALS, ASSESSMENTS, RESULTS OR DATA THROUGH THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES OR ANY THIRD-PARTY SITE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR PROPERTY (INCLUDING YOUR COMPUTER SYSTEM) OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OR USE OF SUCH MATERIAL OR DATA.  </p>

<h2 id="limitationofliabilityanddamages"><strong>18. LIMITATION OF LIABILITY AND DAMAGES</strong></h2>

<h3 id="181limitationofliability"><strong>18.1 Limitation of Liability</strong></h3>

<p>UNDER NO CIRCUMSTANCES, INCLUDING, NEGLIGENCE, WILL THE RECOLUDE PARTIES BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, CONSEQUENTIAL, PUNITIVE, RELIANCE, OR EXEMPLARY DAMAGES (INCLUDING WITHOUT LIMITATION DAMAGES ARISING FROM ANY UNSUCCESSFUL COURT ACTION OR LEGAL DISPUTE, LOST BUSINESS, LOST REVENUES OR LOSS OF ANTICIPATED PROFITS OR ANY OTHER PECUNIARY OR NON-PECUNIARY LOSS OR DAMAGE OF ANY NATURE WHATSOEVER) ARISING OUT OF OR RELATING TO THESE TERMS OR THAT RESULT FROM YOUR USE OR YOUR INABILITY TO USE THE RECOLUDE PLATFORM, THE SITE OR ANY OF THE OTHER RECOLUDE SERVICES OR ANY THIRD-PARTY SITE, OR ANY OTHER INTERACTIONS WITH RECOLUDE OR ANY OTHER DEVELOPER OR USER, EVEN IF RECOLUDE OR A RECOLUDE AUTHORIZED REPRESENTATIVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. APPLICABLE LAW MAY NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY OR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. IN SUCH CASES, RECOLUDE’S LIABILITY WILL BE LIMITED TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW.  </p>

<h3 id="182limitationofdamages"><strong>18.2 Limitation of Damages</strong></h3>

<p>IN NO EVENT WILL THE RECOLUDE PARTIES’ TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION ARISING OUT OF OR RELATING TO YOUR ACCESS AND USE OF THE RECOLUDE PLATFORM, THE SITE OR ANY OF THE OTHER RECOLUDE SERVICES OR THESE TERMS, OR YOUR INTERACTION WITH OTHER USERS (WHETHER IN CONTRACT, TORT INCLUDING NEGLIGENCE, WARRANTY, OR OTHERWISE), EXCEED THE GREATER OF (A) $25 AND (B) AN AMOUNT EQUAL TO THE SUBSCRIPTION FEES PAID BY YOU DURING THE TWELVE (12) MONTHS IMMEDIATELY PRECEDING THE DATE OF THE CLAIM.  </p>

<h3 id="183releasefordisputesbetweenusers"><strong>18.3 Release for Disputes between Users</strong></h3>

<p>If you have a dispute with any other Users (including any other Developer) or other third parties, you hereby release Recolude and the other Recolude Parties from claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, arising out of or in any way connected with such disputes. If you are a California resident, you waive California Civil Code Section 1542, which says: “A general release does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the release, which if known by him must have materially affected his settlement with the debtor.”  </p>

<h3 id="184thirdpartysites"><strong>18.4 Third-party Sites</strong></h3>

<p>THESE LIMITATIONS OF LIABILITY ALSO APPLY WITH RESPECT TO DAMAGES INCURRED BY YOU BY REASON OF ANY PRODUCTS OR SERVICES SOLD OR PROVIDED ON ANY THIRD-PARTY SITES OR OTHERWISE BY THIRD PARTIES OTHER THAN RECOLUDE AND RECEIVED THROUGH OR ADVERTISED ON ANY OF THE RECOLUDE SERVICES OR RECEIVED THROUGH ANY THIRD-PARTY SITES.  </p>

<h3 id="185basisofthebargain"><strong>18.5 Basis of the Bargain</strong></h3>

<p>YOU ACKNOWLEDGE AND AGREE THAT RECOLUDE HAS OFFERED THE RECOLUDE PLATFORM, THE SITE AND THE OTHER RECOLUDE SERVICES, SET ITS PRICES, AND ENTERED INTO THESE TERMS IN RELIANCE UPON THE WARRANTY DISCLAIMERS AND THE LIMITATIONS OF LIABILITY SET FORTH HEREIN, THAT THE WARRANTY DISCLAIMERS AND THE LIMITATIONS OF LIABILITY SET FORTH HEREIN REFLECT A REASONABLE AND FAIR ALLOCATION OF RISK BETWEEN YOU AND RECOLUDE, AND THAT THE WARRANTY DISCLAIMERS AND THE LIMITATIONS OF LIABILITY SET FORTH HEREIN FORM AN ESSENTIAL BASIS OF THE BARGAIN BETWEEN YOU AND RECOLUDE. RECOLUDE WOULD NOT BE ABLE TO AUTHORIZE YOU TO ACCESS AND USE OF THE RECOLUDE PLATFORM OR ANY OF THE OTHER RECOLUDE SERVICES TO YOU ON AN ECONOMICALLY REASONABLE BASIS WITHOUT THESE LIMITATIONS.  </p>

<h3 id="186limitationsbyapplicablelaw"><strong>18.6 Limitations by Applicable Law</strong></h3>

<p>CERTAIN JURISDICTIONS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF YOU RESIDE IN SUCH A JURISDICTION, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MAY HAVE ADDITIONAL RIGHTS. THE LIMITATIONS OR EXCLUSIONS OF WARRANTIES, REMEDIES OR LIABILITY CONTAINED IN THESE TERMS APPLY TO YOU TO THE FULLEST EXTENT SUCH LIMITATIONS OR EXCLUSIONS ARE PERMITTED UNDER THE LAWS OF THE JURISDICTION WHERE YOU ARE LOCATED.  </p>

<h2 id="19exportcontrols"><strong>19. Export Controls</strong></h2>

<p>You agree not to import, export, re-export, or transfer, directly or indirectly, any part of the Recolude Platform or other Recolude Services or any underlying intellectual property, information or technology except in full compliance with all United States, foreign and other applicable export control laws and regulations.  </p>

<h2 id="20miscellaneous"><strong>20. Miscellaneous</strong></h2>

<h3 id="201notice"><strong>20.1 Notice</strong></h3>

<p>Recolude may provide you with notices, including those regarding changes to these Terms, by email, regular mail or postings on any of the Recolude Services. Notice will be deemed given twenty-four hours after the email is sent, unless Recolude is notified that the email address is invalid. Alternatively, Recolude may give you legal notice by mail to a postal address, if provided by you through any of the Recolude Services. In such case, notice will be deemed given three days after the date of mailing. Notices posted on any of the Recolude Services are deemed given 30 days following the initial posting.  </p>

<h3 id="202waiver"><strong>20.2 Waiver</strong></h3>

<p>The failure of Recolude to exercise or enforce any right or provision of these Terms will not constitute a waiver of such right or provision. Any waiver of any provision of these Terms will be effective only if in writing and signed by Recolude.  </p>

<h3 id="203disputeresolution"><strong>20.3 Dispute Resolution</strong></h3>

<p>If a dispute arises between you and Recolude, the goal is to provide you with a neutral and cost-effective method of resolving the dispute quickly. Accordingly, you and Recolude agree that any dispute, claim or controversy at law or equity that arises out of these Terms, the Recolude Platform, the Site, or the other Recolude Services (a “Dispute”) will be resolved in accordance with this Section 20.3 or as Recolude and you otherwise agree in writing. Before resorting to these dispute methods, Recolude strongly encourages you to first contact Recolude directly to seek a resolution.  </p>

<p><strong>(a) Choice of Law</strong></p>

<p>These Terms shall be governed in all respects by the laws of the State of Texas, without regard to its conflict of law provisions. EACH PARTY IRREVOCABLY AND UNCONDITIONALLY WAIVES, TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, ANY RIGHT IT MAY HAVE TO A TRIAL BY JURY IN ANY LEGAL ACTION, PROCEEDING, CAUSE OF ACTION OR COUNTERCLAIM ARISING OUT OF OR RELATING TO ANY DISPUTE OR OTHERWISE IN CONNECTION WITH THESE TERMS OR ANY OF THE RECOLUDE SERVICES.  </p>

<p><strong>(b) Arbitration and Class Action Waiver</strong>  </p>

<p>PLEASE REVIEW AS THIS AFFECTS YOUR LEGAL RIGHTS.  </p>

<p>(i) ARBITRATION. YOU AGREE THAT ALL DISPUTES BETWEEN YOU AND RECOLUDE (WHETHER OR NOT SUCH DISPUTE INVOLVES A THIRD PARTY) IN CONNECTION WITH THESE TERMS OR ANY OF THE RECOLUDE SERVICES, INCLUDING WITHOUT LIMITATION YOUR RIGHTS OF PRIVACY WILL BE RESOLVED BY BINDING, INDIVIDUAL ARBITRATION UNDER THE AMERICAN ARBITRATION ASSOCIATION’S RULES FOR ARBITRATION AND YOU AND RECOLUDE HEREBY EXPRESSLY WAIVE TRIAL BY JURY. ANY ARBITRATION SHALL BE IN ENGLISH AND THE PLACE OF ARBITRATION SHALL BE HOUSTON, TEXAS. YOUR ARBITRATION FEES AND YOUR SHARE OF ARBITRATOR COMPENSATION SHALL BE GOVERNED BY SUCH RULES. DISCOVERY AND RIGHTS TO APPEAL IN ARBITRATION ARE GENERALLY MORE LIMITED THAN IN A LAWSUIT, AND OTHER RIGHTS THAT YOU AND RECOLUDE WOULD HAVE IN COURT MAY NOT BE AVAILABLE IN ARBITRATION. YOU MAY BRING DISPUTES ONLY ON YOUR OWN BEHALF.  </p>

<p>Neither you nor Recolude will participate in a class action or class-wide arbitration for any disputes covered by these Terms to arbitrate. YOU ARE GIVING UP YOUR RIGHT TO PARTICIPATE AS A CLASS REPRESENTATIVE OR CLASS MEMBER ON ANY CLASS CLAIM YOU MAY HAVE AGAINST RECOLUDE INCLUDING ANY RIGHT TO CLASS ARBITRATION OR ANY CONSOLIDATION OF INDIVIDUAL ARBITRATIONS. You also agree not to participate in claims brought in a private attorney general or representative capacity, or consolidated claims involving another person’s account, if Recolude is a party to the proceeding.  </p>

<p>This dispute resolution provision will be governed by the Federal Arbitration Act and not by any state law concerning arbitration. The arbitration may be conducted in person, through the submission of documents, by phone or online. The arbitrator must follow applicable law, and any award may be challenged if the arbitrator fails to do so. Judgment on the award rendered by the arbitrator may be entered in any court having competent jurisdiction. Any provision of applicable law notwithstanding, the arbitrator will not have authority to award damages, remedies or awards that conflict with these Terms.  </p>

<p>(ii) Judicial Forum for Disputes. In the event that the agreement to arbitrate under Section 20.3(b) is found not to apply to you or your claim, you and Recolude agree that any judicial proceeding (other than small claims actions) must be brought solely and exclusively in, and will be subject to the service of process and other applicable procedural rules of, the federal or state courts covering Houston, TX. Both you and Recolude irrevocably consent to venue and personal jurisdiction there. Notwithstanding the foregoing, Recolude may bring a claim for equitable relief in any court with proper jurisdiction.  </p>

<p>(iii) Survival. This arbitration agreement will survive the termination of your use of the Recolude Platform or any of the other Recolude Services or your relationship with Recolude.  </p>

<p><strong>(c) 30 Day Right to Opt-Out</strong></p>

<p>You have the right to opt-out and not be bound by the arbitration provisions set forth in this Section 20.3 above by sending written notice of your decision to opt-out to support@recolude.com. The notice must be sent to Recolude within thirty (30) days of your use of any of the Recolude Services or agreeing to these Terms, otherwise you shall be bound to arbitrate disputes in accordance with the terms of those sections. If you opt-out of these arbitration provisions, we also will not be bound by them.  </p>

<p><strong>(d)Improperly Filed Claims</strong></p>

<p>All claims you bring against Recolude must be resolved in accordance with this Section 20.3. All claims filed or brought contrary to this Section 20.3 shall be considered improperly filed. Should you file a claim contrary to this Section 20.3, Recolude may recover attorneys’ fees and costs up to $15,000, provided that Recolude has notified you in writing of the improperly filed claim, and you have failed to promptly withdraw the claim.  </p>

<p><strong>(e) Prevailing Party</strong></p>

<p>In the event that either party institutes any legal suit, action or proceeding against the other party arising out of or relating to these Terms, the Privacy Policy, or any of the other Recolude Services, the prevailing party in the suit, action or proceeding shall be entitled to receive in addition to all other damages to which it may be entitled, the costs incurred by such party in conducting the suit, action or proceeding, including reasonable attorneys’ fees and expenses and court costs.  </p>

<p><strong>(f) Limitation on Time to File Claims</strong></p>

<p>ANY CAUSE OF ACTION OR DISPUTE YOU MAY HAVE ARISING OUT OF OR RELATING TO THESE TERMS, THE PRIVACY POLICY, ANY OF THE RECOLUDE SERVICES OR YOUR RELATIONSHIP WITH RECOLUDE MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, SUCH CAUSE OF ACTION OR DISPUTE IS PERMANENTLY BARRED.  </p>

<h3 id="204severability"><strong>20.4 Severability</strong></h3>

<p>If any provision of these Terms (including any Policies) is held to be unlawful, void, or for any reason unenforceable, then that provision will be limited or eliminated from these Terms to the minimum extent necessary and will not affect the validity and enforceability of any remaining provisions.  </p>

<h3 id="205relationshipoftheparties"><strong>20.5 Relationship of the Parties</strong></h3>

<p>The parties agree that nothing in these Terms shall be construed as creating a joint venture, partnership, franchise, agency, employer/employee, or similar relationship between the parties, or as authorizing either party to act as the agent of the other. You are and will remain an independent contractor in your relationship to Recolude. Nothing in these Terms shall create any obligation between either party and a third party.  </p>

<h3 id="206assignment"><strong>20.6 Assignment</strong></h3>

<p>These Terms and related Policies, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by Recolude without restriction. Any assignment attempted to be made in violation of these Terms shall be void.  </p>

<h3 id="207survival"><strong>20.7 Survival</strong></h3>

<p>Upon termination of these Terms, your use of the Site, any of the other Recolude Services or your relationship with Recolude any provision which, by its nature or express terms should survive, will survive such termination or expiration, including, but not limited to, Sections 2, 4.4 and 9 – 20.  </p>

<h3 id="208headings"><strong>20.8 Headings</strong></h3>

<p>The heading references herein are for convenience purposes only, do not constitute a part of these Terms, and will not be deemed to limit or affect any of the provisions hereof.  </p>

<h3 id="209entireagreement"><strong>20.9 Entire Agreement</strong></h3>

<p>These Terms, together with the Privacy Policy and the Policies, are the entire agreement between you and Recolude relating to the subject matter herein and will not be modified except by a change to these Terms or Policies made by Recolude as set forth in Section 8 above.  </p>

<h3 id="2010noagency"><strong>20.10 No Agency</strong></h3>

<p>No agency, partnership, joint venture, employee-employer or franchiser-franchisee relationship is intended or created by these Terms.  </p>

<h3 id="2011geographicrestrictions"><strong>20.11 Geographic Restrictions</strong></h3>

<p>Recolude is based in the state of Texas in the United States. Recolude makes no claims that accessing or using or that any of the Recolude Services or any of the content is accessible or appropriate outside of the United States. Accessing or using the Recolude Platform, the Site or any of the other Recolude Services may not be legal by certain persons or in certain countries. If you access any of these Recolude Services from outside the United States, you do so on your own initiative and are responsible for compliance with local laws and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against Recolude with respect thereto.  </p>

<h3 id="2012electroniccommunications"><strong>20.12 Electronic Communications</strong></h3>

<p>The communications between you and Recolude use electronic means, whether you visit the Site or the other Recolude Services or send Recolude e-mails, or whether Recolude posts notices on the Site or the other Recolude Services or communicates with you via e-mail. For contractual purposes, you (a) consent to receive communications from Recolude in an electronic form; and (b) agree that all terms, conditions, agreements, notices, disclosures, and other communications that Recolude provides to you electronically satisfy any legal requirement that such communications would satisfy if they were in writing. The foregoing does not affect your statutory rights.  </p>

<h3 id="2013disclosures"><strong>20.13 Disclosures</strong></h3>

<p>The Recolude Platform, the Site and the other Recolude Services are offered by Recolude, LLC, PO Box 9826, Spring, TX 77387 and email: support@recolude.com.  </p>

<p>If you are a California resident, you may have this same information emailed to you by sending a letter to Recolude, LLC, PO Box 9826, Spring, TX 77387 with your email address and a request for this information.  </p>

<p>California users are also entitled to the following specific consumer rights notice: The Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs may be contacted in writing at 1625 North Market Blvd., Sacramento, CA 95834, or by telephone at (916) 445-1254 or (800) 952-5210.  </p>

<p><strong>© 2020 Recolude, LLC</strong></p>`
